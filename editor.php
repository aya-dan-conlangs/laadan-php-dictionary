<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Láadan Dictionary - Editable </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="assets/style.css">
  <link rel="icon" type="image/png" href="assets/favicon.png">

  <script src="assets/jquery-3.5.0.min.js"></script>
  <script src="assets/script.js"></script>
</head>

<body>
    
<? include_once( "backend.php" ); ?>

<pre class="debug">
LOG
<? print_r( $dictionary->log ); ?>
POST 
<? print_r( $_POST ); ?>
DICT
<? foreach( $dictionary->GetDictionary() as $key => $value ) {
    print_r( $value );
    break;
} ?>
</pre>


<div class="dictionary-view">    
    <div class="results-view" id="results-view"> 
            
            <h2>Add new word</h2>
            <form method="post" name="dictionary-add">
                <div class="dictionary-entry-editable cf">
                    
                    <div class="column cf">
                        <p class="laadan">
                            <sub>Láadan</sub><br>
                            <input type="text" name="item[new][láadan]" value="<?=$item["láadan"]?>">
                        </p>
                        <p class="language">
                            <sub>English</sub><br>
                            <input type="text" name="item[new][english][translation]" value="<?=$item["english"]["translation"]?>">
                        </p>
                        <p class="language">
                            <sub>Español</sub><br>
                            <input type="text" name="item[new][español][translation]" value="<?=$item["español"]["translation"]?>">
                        </p>
                    </div>
                    <p class="description">
                        <sub>Description</sub><br>
                        <input type="text" name="item[new][english][description]" value="<?=$item["english"]["description"]?>">
                    </p>
                    <div class="column cf">
                        <p class="breakdown">
                            <sub>Breakdown</sub><br>
                            <input type="text" name="item[new][english][breakdown]" value="<?=$item["english"]["breakdown"]?>">
                        </p>
                        <p class="notes">
                            <sub>Notes</sub><br>
                            <input type="text" name="item[new][english][notes]" value="<?=$item["english"]["notes"]?>">
                        </p>
                        <p class="classification">
                            <sub>Classification</sub><br>
                            <input type="text" name="item[new][english][classification]" value="<?=$item["english"]["classification"] ?>">
                        </p>
                    </div>
                    
                    <p><input type="submit" name="dictionary-add" value="Add new word" class="form-submit"></p>
                </div>
            </form>
            
            <h2>Existing words</h2>
            <? foreach( $dictionary->GetDictionary() as $key => $item ) { ?>
                <form method="post" name="dictionary-update">       
                    
                    <div class="dictionary-entry-editable cf">
                        
                        <div class="column cf">
                            <p class="laadan">
                                <sub>Láadan</sub><br>
                                <input type="text" name="item[<?=$key?>][láadan]" value="<?=$item["láadan"]?>">
                            </p>
                            <p class="language">
                                <sub>English</sub><br>
                                <input type="text" name="item[<?=$key?>][english][translation]" value="<?=$item["english"]["translation"]?>">
                            </p>
                            <p class="language">
                                <sub>Español</sub><br>
                                <input type="text" name="item[<?=$key?>][español][translation]" value="<?=$item["español"]["translation"]?>">
                            </p>
                        </div>
                        <p class="description">
                            <sub>Description</sub><br>
                            <input type="text" name="item[<?=$key?>][english][description]" value="<?=$item["english"]["description"]?>">
                        </p>
                        <div class="column cf">
                            <p class="breakdown">
                                <sub>Breakdown</sub><br>
                                <input type="text" name="item[<?=$key?>][english][breakdown]" value="<?=$item["english"]["breakdown"]?>">
                            </p>
                            <p class="notes">
                                <sub>Notes</sub><br>
                                <input type="text" name="item[<?=$key?>][english][notes]" value="<?=$item["english"]["notes"]?>">
                            </p>
                            <p class="classification">
                                <sub>Classification</sub><br>
                                <input type="text" name="item[<?=$key?>][english][classification]" value="<?=$item["english"]["classification"] ?>">
                            </p>
                        </div>
                        
                        <p><input type="submit" name="dictionary-update" value="Save <?=$item["láadan"]?>" class="form-submit"></p>
                        <p><input type="submit" name="dictionary-delete" value="Delete <?=$item["láadan"]?>" class="delete-word"></p>
                    </div>
                </form>
            <? } ?>
        
    </div>
</div>

</body>
</html>
